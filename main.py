#!/usr/bin/env python3

from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine

if __name__ == "__main__":
    app = QGuiApplication()
    engine = QQmlApplicationEngine()

    context = engine.rootContext()
    engine.load("qml/main.qml")

    if len(engine.rootObjects()) == 0:
        quit()
    app.exec_()
